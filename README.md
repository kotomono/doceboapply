# doceboApply

git clone https://gitlab.com/kotomono/doceboapply.git

npm install

react-native link


react-native run-android

or

react-native run-ios


Notes: 
- App consists in two pages: Search and Result. In Search page you can type a text and select a course name from a dropdown menu; none of the parameters are required to press the search button. In Result page there are two different dropdown menus: "type" one will let you filter the results by type (extracted from the received list), "sort" one will let you sort the list alphabetically. To reset the filters you have to select "all" (for type) and "default" (for sort); for convenience, I disabled the lazy loading of the list while filter type != 'all' and sort != 'default'; simply scroll down and reach the end of the list to load more items, if available.
- To swap between dark and light mode just press the icon on top-right of the screen (moon/sun)


Creator: Roberto Bergamini

contacts: rob.berga@gmail.com | +393474921613