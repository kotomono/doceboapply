import { stripHtmlRegex } from "../constants/regex"

export const currencyFormat = (price, currency) => {
    return price + " " + currency;
}

export const stripHtml = (text) => {
    return text.replace(stripHtmlRegex, '');
}
