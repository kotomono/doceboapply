import { colors } from "../constants/colors";

export const getColor = (darkMode, color) => {
    switch (color) {
        case "primary":
            return darkMode ? colors.white : colors.black;
        case "secondary":
            return darkMode ? colors.dark : colors.light;
        case "grey":
            return darkMode ? colors.greyLight : colors.greyDark;
        default:
            return darkMode ? colors.white : colors.black;
    }
}

export const getIcon = (darkMode) => {
    return darkMode ? "ios-sunny" : "ios-moon";
}