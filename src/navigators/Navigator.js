import Search from '../screens/Search/Search.container'
import Result from '../screens/Result/Result.container'

import {
    createStackNavigator,
    createAppContainer
} from 'react-navigation';

const NavigatorStack = createStackNavigator({
    Search: {
        screen: Search,
        navigationOptions: {
            header: null
        }
    },
    Result: {
        screen: Result,
        navigationOptions: {
            header: null
        }
    }
});

const Navigator = createAppContainer(NavigatorStack);

export default Navigator;