import {
    TOGGLE_DARK_MODE,
} from "./types";

export const toggleDarkMode = () => {
    return async (dispatch) => {
            dispatch({ type: TOGGLE_DARK_MODE });
    };
};