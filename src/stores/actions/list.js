import { BASE_URL } from "../../config/strings"
import {
    GET_LIST_REQUEST,
    GET_LIST_FAILURE,
    GET_LIST_SUCCESS,
    CONCAT_LIST_REQUEST,
    CONCAT_LIST_FAILURE,
    CONCAT_LIST_SUCCESS
} from "./types";

const config = {
    method: "GET",
    headers: {
        Accept: "application/json"
    }
};

export const fetchGetList = ({search_text, type}, cb) => {
    return async (dispatch) => {
        try {
            dispatch({ type: GET_LIST_REQUEST });
            const URL = BASE_URL + "catalog?type[]=" + type + "&search_text=" + search_text;
            const response = await fetch(
                URL,
                config
            );
            const data = await response.json();
            if (cb) {
                cb(data, null);
            }
            dispatch({
                type: GET_LIST_SUCCESS,
                items: data.data.items,
                current_page: data.data.current_page,
                total_page_count: data.data.total_page_count
            });
        } catch (error) {
            dispatch({
                type: GET_LIST_FAILURE,
                httpError: error
            });
            if (cb) {
                cb(null, error);
            }
        }
    };
};

export const fetchConcatList = ({search_text, type, page}, cb) => {
    return async (dispatch) => {
        try {
            dispatch({ type: CONCAT_LIST_REQUEST });
            const URL = BASE_URL + "catalog?type[]=" + type + "&search_text=" + search_text + "&page=" + page;
            const response = await fetch(
                URL,
                config
            );
            const data = await response.json();
            if (cb) {
                cb(data, null);
            }
            dispatch({
                type: CONCAT_LIST_SUCCESS,
                items: data.data.items,
                current_page: data.data.current_page,
                total_page_count: data.data.total_page_count
            });
        } catch (error) {
            dispatch({
                type: CONCAT_LIST_FAILURE,
                httpError: error
            });
            if (cb) {
                cb(null, error);
            }
        }
    };
};