
import { createStore, applyMiddleware } from 'redux';
import reducer from "./reducers/index";
import thunk from "redux-thunk"
const {
  fetchGetList,
  fetchConcatList,
  toggleDarkMode
} = require("./actions");
const store = createStore(reducer, applyMiddleware(thunk));

module.exports = {
  fetchGetList,
  fetchConcatList,
  toggleDarkMode,
  store
};
