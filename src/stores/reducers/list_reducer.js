import {
  GET_LIST_REQUEST,
  GET_LIST_FAILURE,
  GET_LIST_SUCCESS,
  CONCAT_LIST_REQUEST,
  CONCAT_LIST_FAILURE,
  CONCAT_LIST_SUCCESS
} from "../actions/types";

const initialState = {
  items: [],
  current_page: null,
  total_page_count: null,
  httpError: "",
  isFetching: false,
  isFetchingConcat: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LIST_REQUEST:
      return {
        ...state,
        isFetching: true
      };
    case GET_LIST_SUCCESS:
      return {
        ...state,
        items: action.items,
        current_page: action.current_page,
        total_page_count: action.total_page_count,
        isFetching: false
      };
    case GET_LIST_FAILURE:
      return {
        ...state,
        httpError: action.httpError,
        isFetching: false
      };
    case CONCAT_LIST_REQUEST:
      return {
        ...state,
        isFetchingConcat: true
      };
    case CONCAT_LIST_SUCCESS:
      const concatList = {};
      concatList.items = state.items.concat(action.items);
      return {
        ...state,
        items: concatList.items,
        current_page: action.current_page,
        total_page_count: action.total_page_count,
        isFetchingConcat: false
      };
    case CONCAT_LIST_FAILURE:
      return {
        ...state,
        httpError: action.httpError,
        isFetchingConcat: false
      };
    default:
      return state;
  }
};
