import list from "./list_reducer";
import darkMode from "./darkMode_reducer";

import { combineReducers } from "redux";

const reducer = combineReducers({
    list,
    darkMode
});

export default reducer;
