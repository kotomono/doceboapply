export const colors = {
    black: "#000000",
    white: "#FFFFFF",
    dark: "#444444",
    light: "#F6F6F6",
    greyLight: "#d4d4d4",
    greyDark: "#696969"
};