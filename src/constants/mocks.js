export const courses = [
    {
        value: 'all',
    },
    {
        value: 'classroom',
    },
    {
        value: 'elearning',
    },
    {
        value: 'mobile',
    },
    {
        value: 'webinar',
    },
    {
        value: 'learning_plan',
    }
];

export const sorts = [
    {
        value: 'default',
    },
    {
        value: 'A to Z',
    },
    {
        value: 'Z to A',
    }
];