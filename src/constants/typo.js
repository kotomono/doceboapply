export const typo = {
    strong: {
        fontWeight: 'bold'
    },
    big: {
        fontSize: 18
    },
    regular: {
        fontSize: 14
    },
    small: {
        fontSize: 12
    }
}