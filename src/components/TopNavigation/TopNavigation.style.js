import { StyleSheet, Platform } from 'react-native'
import { colors } from '../../constants/colors'
import { typo } from '../../constants/typo'

const styles = StyleSheet.create({
  topNavContainer: {
    flexDirection: 'row',
    height: 64
  },
  topNavbarTitle: {
    flex: 0.6,
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    ...typo.strong
  },
  topNavBackIcon: {
    width: 20
  },
  topNavLeft: {
    flex: 0.2,
    justifyContent: "center",
    alignItems: "flex-start"
  },
  topNavRight: {
    flex: 0.2,
    justifyContent: "center",
    alignItems: "flex-end"
  }
})

export default styles