import React from "react";
import { View, Text, TouchableOpacity } from 'react-native';
import styles from './TopNavigation.style';
import { colors } from "../../constants/colors";
import Icon from 'react-native-vector-icons/Ionicons';

const TopNavigation = props => {
    const { back, title, rightIcon, onBackPress, onRightPress, primaryColor, secondaryColor } = props;
    return (
        <View style={[styles.topNavContainer, {backgroundColor: secondaryColor}]}>
            <View style={styles.topNavLeft}>
                {back ?
                    <TouchableOpacity onPress={onBackPress}>
                        <Icon name={'ios-arrow-back'} size={24} color={primaryColor}/>
                    </TouchableOpacity>
                    : null}
            </View>
            <View style={styles.topNavbarTitle}>
                <Text style={[styles.title, {color: primaryColor}]}>{title}</Text>
            </View>
            <View style={styles.topNavRight}>
            {rightIcon ?
                    <TouchableOpacity onPress={onRightPress}>
                        <Icon name={rightIcon} size={24} color={primaryColor}/>
                    </TouchableOpacity>
                    : null}
            </View>
        </View>
    )
}

export default TopNavigation;


