import React from 'react'
import styles from './Divider.style'
import { View } from 'react-native'

const Divider = (props) => {
    const { color } = props
    return (
        <View style={[styles.divider, {backgroundColor: color}]} />
    )
}

export default Divider
