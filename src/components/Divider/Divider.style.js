import { StyleSheet } from 'react-native'
import { colors } from '../../constants/colors'

const styles = StyleSheet.create({
  divider: {
    width: '100%',
    height: 0.8
  }
})

export default styles