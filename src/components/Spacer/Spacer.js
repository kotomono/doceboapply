import React from "react";
import { View } from "react-native";

const Spacer = props => {
    const { height, width } = props;
    return (
        <View style={{
            height: height ? height : 0,
            width: width ? width : 0,
        }} />
    );
};

export default Spacer;
