import React from "react";
import { View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import styles from './PickerContainer.style';
import { colors } from "../../constants/colors";

const PickerContainer = props => {
    const { title, data, defaultValue, count, onChangeValuePicker, primaryColor, secondaryColor } = props;
    return (
        <View style={styles.pickerContainer}>
            <Dropdown
                baseColor={primaryColor}
                textColor={primaryColor}
                selectedItemColor={colors.black}
                itemColor={colors.greyLight}
                itemCount={count}
                label={title}
                data={data}
                value={defaultValue}
                dropdownPosition={0}
                onChangeText={onChangeValuePicker}
            />
        </View>
    )
}

export default PickerContainer;


