import { StyleSheet } from 'react-native'
import { colors } from '../../constants/colors'

const styles = StyleSheet.create({
  container: {
    flexDirection: "column"
  },
  titleContainer: {
    justifyContent: "center"
  },
  title: {
  },
  pickerContainer: {
  }
})

export default styles