import { StyleSheet } from 'react-native'
import { colors } from '../../constants/colors'
import { typo } from "../../constants/typo"

const styles = StyleSheet.create({
    container: {
        height: 40,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: "center",
        justifyContent: "center"
    },
    text: {
        ...typo.strong
    }
})

export default styles