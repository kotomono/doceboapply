import React from "react";
import { View, TouchableOpacity, Text } from "react-native";
import styles from "./ButtonCustom.style";

const ButtonCustom = props => {
    const { title, onPress, primaryColor, secondaryColor } = props;
    return (
        <TouchableOpacity style={[styles.container, {borderColor: primaryColor}]} onPress={onPress}>
            <Text style={[styles.text, {color: primaryColor}]}>{title}</Text>
        </TouchableOpacity>
    );
};

export default ButtonCustom;
