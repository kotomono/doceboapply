import { connect } from "react-redux";
import Search from "./Search";

import {
  fetchGetList,
  toggleDarkMode
} from "../../stores";

mapStateToProps = state => ({
  list: state.list,
  theme: state.darkMode
});

mapDispatchToProps = dispatch => ({
    fetchGetList: (params, cb) => dispatch(fetchGetList(params, cb)),
    toggleDarkMode: () => dispatch(toggleDarkMode())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search);
