import React, { Component } from "react";
import {
  TextInput
} from "react-native";
import { SafeAreaView } from "react-navigation";
import styles from "./Search.style";
import Spacer from "../../components/Spacer/Spacer";
import PickerContainer from "../../components/PickerContainer/PickerContainer";
import ButtonCustom from "../../components/ButtonCustom/ButtonCustom";
import TopNavigation from "../../components/TopNavigation/TopNavigation";
import Divider from "../../components/Divider/Divider";
import { courses } from "../../constants/mocks";
import { getColor, getIcon } from "../../utils/theme";
import { SEARCH_TITLE, BUTTON_SEARCH_TEXT, PLACEHOLDER_ITEM_NAME, PLACEHOLDER_COURSE_TYPE } from "../../constants/text";

export default class Search extends Component {
  state = {
    params: {
      search_text: null,
      type: 'all'
    }
  };

  toggleTheme = () => {
    this.props.toggleDarkMode();
  }

  onChangeText = (text) => {
    const params = { ...this.state.params };
    params.search_text = text;
    this.setState({ params });
  }

  onChangeItem = (text) => {
    const params = { ...this.state.params };
    params.type = text;
    this.setState({ params });
  }

  goToResult = () => {
    this.props.navigation.navigate("Result", { search: this.state.params });
  }

  render() {
    const primaryColor = getColor(this.props.theme.darkMode, "primary");
    const secondaryColor = getColor(this.props.theme.darkMode, "secondary");
    const greyColor = getColor(this.props.theme.darkMode, "grey");

    return (
      <SafeAreaView style={[styles.mainContainer, { backgroundColor: secondaryColor }]}>
        <TopNavigation
          title={SEARCH_TITLE}
          rightIcon={getIcon(this.props.theme.darkMode)}
          onRightPress={this.toggleTheme}
          primaryColor={primaryColor}
          secondaryColor={secondaryColor}
        />
        <Divider color={primaryColor} />
        <Spacer height={20} />
        <TextInput
          style={[styles.input, { borderColor: primaryColor, color: primaryColor }]}
          onChangeText={this.onChangeText}
          value={this.state.params.search_text}
          placeholder={PLACEHOLDER_ITEM_NAME}
          placeholderTextColor={greyColor}
        />
        <PickerContainer
          title={PLACEHOLDER_COURSE_TYPE}
          defaultValue={courses[0].value}
          count={6}
          data={courses}
          onChangeValuePicker={this.onChangeItem}
          primaryColor={primaryColor}
          secondaryColor={secondaryColor}
        />
        <Spacer height={20} />
        <ButtonCustom
          title={BUTTON_SEARCH_TEXT}
          onPress={this.goToResult}
          primaryColor={primaryColor}
          secondaryColor={secondaryColor}
        />
      </SafeAreaView>
    );
  }
}