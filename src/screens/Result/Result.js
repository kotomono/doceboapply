import React, { Component } from "react";
import {
    View,
    Text,
    ActivityIndicator
} from "react-native";
import { SafeAreaView } from "react-navigation";
import styles from "./Result.style";
import Spacer from "../../components/Spacer/Spacer";
import { sorts } from "../../constants/mocks";
import Header from "./components/Header";
import List from "./components/List";
import { getColor, getIcon } from "../../utils/theme";
import TopNavigation from "../../components/TopNavigation/TopNavigation";
import Divider from "../../components/Divider/Divider";
import { ERROR_GENERIC, RESULT_TITLE } from "../../constants/text";

export default class Result extends Component {
    state = {
        params: null,
        types: null,
        selectedType: null,
        selectedSort: null
    };

    componentDidMount() {
        const params = this.props.navigation.getParam("search");
        this.setState({ params }, () => {
            this.props.fetchGetList(this.state.params);
        })
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props && prevProps.theme.darkMode === this.props.theme.darkMode) {
            this.fillTypes();
        }
    }

    fillTypes = () => {
        if (this.props.list.items.length > 0) {
            const types = [{ value: 'all' }];
            const item_types = [...new Set(this.props.list.items.map(item => item.item_type))];
            for (const item_type of item_types) {
                types.push({ value: item_type });
            }
            this.setState({ types, selectedType: this.state.selectedType ? this.state.selectedType : types[0].value });
        }
    }

    toggleTheme = () => {
        this.props.toggleDarkMode();
    }

    onChangeItem = (value) => {
        this.setState({ selectedType: value })
    }

    onChangeSort = (value) => {
        this.setState({ selectedSort: value })
    }

    filterList = () => {
        if (this.props.list && this.props.list.items) {
            const filtered = this.props.list.items.filter(item => {
                return (
                    this.state.selectedType && this.state.selectedType != 'all' ? item.item_type === this.state.selectedType : true
                );
            });

            switch (this.state.selectedSort) {
                case "A to Z":
                    return filtered.sort(function (a, b) {
                        if (a.item_name < b.item_name) { return -1; }
                        if (a.item_name > b.item_name) { return 1; }
                        return 0;
                    })
                case "Z to A":
                    return filtered.sort(function (a, b) {
                        if (a.item_name < b.item_name) { return 1; }
                        if (a.item_name > b.item_name) { return -1; }
                        return 0;
                    })
                default:
                    return filtered;
            }
        }
    }

    onEndReached = () => {
        if (this.props.list.current_page < this.props.list.total_page_count &&
            !this.props.list.isFetchingConcat &&
            (this.state.selectedType == null || this.state.selectedType == "all") &&
            (this.state.selectedSort == null || this.state.selectedSort == "default")) {
            const params = { ...this.state.params };
            params.page = this.props.list.current_page + 1;
            this.setState({ params }, () => {
                this.props.fetchConcatList(this.state.params);
            });
        }
    }

    goBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        const primaryColor = getColor(this.props.theme.darkMode, "primary");
        const secondaryColor = getColor(this.props.theme.darkMode, "secondary");

        return (
            <SafeAreaView style={[styles.mainContainer, { backgroundColor: secondaryColor }]}>
                <TopNavigation
                    back
                    onBackPress={this.goBack}
                    title={RESULT_TITLE}
                    rightIcon={getIcon(this.props.theme.darkMode)}
                    onRightPress={this.toggleTheme}
                    primaryColor={primaryColor}
                    secondaryColor={secondaryColor}
                />
                <Divider color={primaryColor} />
                {this.props.list.isFetching ?
                    <View style={styles.loaderContainer}>
                        <ActivityIndicator size="large" color={primaryColor} />
                    </View>
                    : this.props.list.items ?
                        <View style={styles.contentContainer}>
                            <Header
                                dataTypes={this.state.types}
                                dataSort={sorts}
                                onChangeValuePicker={this.onChangeItem}
                                onChangeSortPicker={this.onChangeSort}
                                countItems={this.filterList().length}
                                currentPage={this.props.list.current_page}
                                totalPage={this.props.list.total_page_count}
                                primaryColor={primaryColor}
                                secondaryColor={secondaryColor}
                            />
                            <Spacer height={20} />
                            <List
                                data={this.filterList()}
                                state={this.state}
                                onEndReached={this.onEndReached}
                                primaryColor={primaryColor}
                                secondaryColor={secondaryColor}
                            />
                        </View>
                        : this.props.list.httpError ?
                            <View style={styles.contentContainer}>
                                <Spacer height={20} />
                                <Text style={{ color: primaryColor }}>{ERROR_GENERIC}</Text>
                            </View>
                            : null
                }
            </SafeAreaView>
        );
    }
}