import { connect } from "react-redux";
import Result from "./Result";

import {
  fetchGetList,
  fetchConcatList,
  toggleDarkMode
} from "../../stores";

mapStateToProps = state => ({
  list: state.list,
  theme: state.darkMode
});

mapDispatchToProps = dispatch => ({
  fetchGetList: (params, cb) => dispatch(fetchGetList(params, cb)),
  fetchConcatList: (params, cb) => dispatch(fetchConcatList(params, cb)),
  toggleDarkMode: () => dispatch(toggleDarkMode())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Result);
