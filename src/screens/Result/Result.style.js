import { StyleSheet } from 'react-native';
import { colors } from '../../constants/colors';
import { typo } from "../../constants/typo";

export default styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        paddingHorizontal: 20
    },
    contentContainer: {
        flex: 1
    },
    loaderContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    cellContainer: {
        flexDirection: "row"
    },
    cellLeft: {
        paddingRight: 10,
        justifyContent: "center",
        alignItems: "center"
    },
    cellRight: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center"
    },
    image: {
        width: 100,
        height: 100
    },
    title: {
        ...typo.strong
    },
    description: {
        ...typo.small
    }
})