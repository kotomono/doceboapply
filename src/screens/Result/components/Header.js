import React from "react";
import { View, Text } from "react-native";
import Spacer from "../../../components/Spacer/Spacer";
import PickerContainer from "../../../components/PickerContainer/PickerContainer";
import styles from "../Result.style";
import { OF_TEXT, PAGE_TEXT, ITEMS_TEXT, PLACEHOLDER_TYPE, PLACEHOLDER_SORT } from "../../../constants/text";

const Header = props => {
    const { dataTypes, dataSort, onChangeValuePicker, onChangeSortPicker, countItems, primaryColor, secondaryColor, currentPage, totalPage } = props;
    return (
        <View>
            {dataTypes && dataTypes.length > 0 && countItems > 0 ?
                <PickerContainer
                    title={PLACEHOLDER_TYPE}
                    defaultValue={dataTypes[0].value}
                    count={6}
                    data={dataTypes}
                    onChangeValuePicker={onChangeValuePicker}
                    primaryColor={primaryColor}
                    secondaryColor={secondaryColor}
                />
                : null}
            {dataSort && dataSort.length > 0 && countItems > 0 ?
                <PickerContainer
                    title={PLACEHOLDER_SORT}
                    defaultValue={dataSort[0].value}
                    count={3}
                    data={dataSort}
                    onChangeValuePicker={onChangeSortPicker}
                    primaryColor={primaryColor}
                    secondaryColor={secondaryColor}
                />
                : null}
            <Spacer height={10} />
            <View style={styles.cellContainer}>
                <View style={styles.contentContainer}>
                    <Text style={{ color: primaryColor }}>{countItems}{ITEMS_TEXT}</Text>
                </View>
                {countItems ?
                    <View style={[styles.contentContainer, { alignItems: "flex-end" }]}>
                        <Text style={{ color: primaryColor }}>{PAGE_TEXT}{currentPage}{OF_TEXT}{totalPage}</Text>
                    </View>
                    : null}
            </View>
        </View>
    );
};

export default Header;