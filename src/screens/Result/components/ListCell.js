import React, { PureComponent } from "react";
import { View, Text, Image } from "react-native";
import styles from "../Result.style";
import Divider from "../../../components/Divider/Divider";
import Spacer from "../../../components/Spacer/Spacer";
import { currencyFormat } from "../../../utils/format";
import { PROTOCOL } from "../../../config/strings"
import { stripHtml } from "../../../utils/format";
import { CURRENCY } from "../../../constants/text";

class ListCell extends PureComponent {

    render() {
        const { item, primaryColor } = this.props;
        return (
            <View>
                <View style={styles.cellContainer}>

                    <View style={styles.cellLeft}>
                        {item.item_thumbnail ?
                            <Image
                                style={[styles.image, { backgroundColor: primaryColor }]}
                                source={{ uri: PROTOCOL + item.item_thumbnail }}
                            />
                            :
                            <View style={[styles.image, { backgroundColor: primaryColor }]} />
                        }
                    </View>

                    <View style={styles.cellRight}>
                        {item.item_name ?
                            <Text style={[styles.title, { color: primaryColor }]}>{item.item_name}</Text>
                            : null}
                        <Text style={{ color: primaryColor }}>{item.item_type && item.item_type} | {item.item_price && currencyFormat(item.item_price, CURRENCY)}</Text>
                        {item.item_description ?
                            <Text style={[styles.description, { color: primaryColor }]}>{stripHtml(item.item_description)}</Text>
                            : null}
                    </View>
                </View>
                <Spacer height={10} />
                <Divider color={primaryColor}/>
                <Spacer height={10} />
            </View>
        );
    }
};

export default ListCell;
