import React from "react";
import { FlatList } from "react-native";
import ListCell from "./ListCell";

const List = props => {
    const { data, state, onEndReached, primaryColor, secondaryColor } = props;
    _keyExtractor = (item, index) => item.item_id + "-" + index;

    _renderItem = ({ item }) => (
        <ListCell
            item={item}
            primaryColor={primaryColor}
            secondaryColor={secondaryColor}
        />
    );


    return (
        <FlatList
            data={data}
            extraData={state}
            keyExtractor={this._keyExtractor}
            renderItem={_renderItem}
            onEndReachedThreshold={0.01}
            onEndReached={onEndReached}
        />
    );
};

export default List;
