import React, { Component } from 'react';
import Navigator from "./src/navigators/Navigator";
import { Provider } from 'react-redux';
import { store } from "./src/stores";

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}